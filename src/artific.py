#!/usr/bin/python3
# ******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Author: DisNight
# Create: 2021-08-02
# ******************************************************************************/

import logging
import http.client
import ssl
import re
from lxml.html import etree
from pymaven.pom import Pom

HTTP_STATUS_SUCCESS = 200


class Artifact(object):
    def __init__(self, group_id, artifact_id, version=""):
        self.group_id = group_id
        self.id = artifact_id
        self.version = version
        self.license = ""
        self.homepage = ""
        self.date = ""
        self.snippets = []


class HttpsClient(object):
    HEADERS = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55"
    }

    def __init__(self, host_server, timeout=20):
        self.timeout = timeout
        self.ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
        if not host_server:
            self.conn = None
        else:
            self.conn = http.client.HTTPSConnection(
                host_server, context=self.ctx, timeout=20
            )

    def request_with_default_headers(self, method, url):
        if not self.conn:
            logging.error("host server not exists")
            return None
        self.conn.request(method, url, headers=self.HEADERS)
        return self.conn.getresponse()


# 修改成单例模式，只保存一个客户端
class MvnRepoMetadateApi(object):
    MVN_REPO_HOST_SERVER = "repo1.maven.org"
    URL_PREFIX = "maven2"

    def __init__(self):
        self.httpclient = HttpsClient(self.MVN_REPO_HOST_SERVER, timeout=20)
        self.host = self.MVN_REPO_HOST_SERVER
        self.url_prefix = self.URL_PREFIX

    def get_maven_metadata(self, groupid, artifactid, version=""):
        """
        从远端maven仓库获取maven-metadata文件

        attributes：
            groupid: attribute of jar file
            artifactid: attribute of jar file
            version: attribute of jar file
        return:
            jar file version
        """
        if not groupid or not artifactid:
            logging.error("groupid or artifactid is empty")
            return ""
        tmpgid = groupid.replace(".", "/")
        tmpaid = artifactid.replace(".", "/")
        metadata_url = f"/{self.url_prefix}/{tmpgid}/{tmpaid}/maven-metadata.xml"
        response = self.httpclient.request_with_default_headers("GET", metadata_url)
        if not response.status == 200:
            logging.error(
                f"GET [https://{self.host}/{metadata_url}] failed:[{response.status}]"
            )
            return ""
        html_content = response.read()
        xml_root = etree.fromstring(html_content)
        # version不存在 则获取最新版本号
        if not version:
            # etree Element find() 找到一个tag匹配的元素返回
            if not xml_root.find("version"):
                logging.error(f"cannot find version in [{metadata_url}]")
                return ""
            return xml_root.find("version").text
        # 存在返回原版本号，不存在返回空字符串
        ver_set = set()
        for ver in xml_root.iter("version"):
            ver_set.add(ver.text)
        if not version in ver_set:
            logging.error(f"version:[{version}] not in maven version set")
            return ""
        return version

    def get_pom(
        self,
        groupid,
        artifactid,
        version="",
        skip_ver_check=False,
        save_file=False,
        save_path="",
    ):
        """
        从远端maven仓库获取pom文件

        attributes：
            groupid: attribute of jar file
            artifactid: attribute of jar file
            version: attribute of jar file
            save_file: flag to enable save the pom file
            save_path: the path to save file
        return:
            pom project or None
        """
        if not groupid or not artifactid:
            logging.error("groupid or artifactid is empty")
            return ""
        tmpgid = groupid.replace(".", "/")
        tmpaid = artifactid.replace(".", "/")
        tmpver = (
            version
            if skip_ver_check
            else self.get_maven_metadata(groupid, artifactid, version)
        )
        if not tmpver:
            logging.error(f"cannot get version from maven repo: [{self.host}]")
            return None
        pom_url = f"/{self.url_prefix}/{tmpgid}/{tmpaid}/{tmpver}/{tmpaid}-{tmpver}.pom"
        response = self.httpclient.request_with_default_headers("GET", pom_url)
        if not response.status == 200:
            logging.error(
                f"GET [https://{self.host}/{pom_url}] failed:[{response.status}]"
            )
            return None
        html_content = response.read()
        if save_file:
            with open(f"{tmpaid}-{tmpver}.pom", "a+") as f:
                f.write(html_content.decode("utf-8"))
        return Pom(pom_data=html_content.decode("utf-8"))

    def download_source(self, groupid, artifactid, version=""):
        """
        从远端maven仓库下载源码文件

        attributes：
            groupid: attribute of jar file
            artifactid: attribute of jar file
            version: attribute of jar file
        return:
            pom project or None
        """
        pass


class HWCMvnRepoMetadateApi(MvnRepoMetadateApi):
    MVN_REPO_HOST_SERVER = "repo.huaweicloud.com"
    URL_PREFIX = "/repository/maven"

    def __init__(self):
        self.httpclient = HttpsClient(self.MVN_REPO_HOST_SERVER, timeout=20)
        self.host = self.MVN_REPO_HOST_SERVER
        self.url_prefix = self.URL_PREFIX


class MvnRepositoryAPI(object):
    MVNREPO_HOST_SERVER = "mvnrepository.com"
    ARTIFACT_TEMPLETE = "/artifact/{group_id}/{artifact_id}"
    ARTIFACT_REPO_TEMPLETE = "/artifact/{group_id}/{artifact_id}?repo={repo}"
    headers = {
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55"
    }

    def __init__(self):
        self.httpsclient = HttpsClient(self.MVNREPO_HOST_SERVER, timeout=20)

    def serch_artifact(
        self,
        group_id: str,
        artifact_id: str,
    ):
        """
        parse response from request to ARTIFACT_TEMPLETE, get maven repo which provide the jar

        attritubes:
            group_id: jar group_id
            artifact_id: jar artifact_id

        return:
            repo_list: {
                repo_flag: href: xxx, num: xxx
            }
        """
        if not group_id or not artifact_id:
            logging.error(f"illegal args: gid[{group_id}] aid[{artifact_id}]")
            return {}
        artifact_url = MvnRepositoryAPI.ARTIFACT_TEMPLETE.format(
            group_id=group_id, artifact_id=artifact_id
        )
        response = self.httpsclient.request_with_default_headers("GET", artifact_url)
        # HTTP STATUS CODE is not SUCCESS
        if response.status != HTTP_STATUS_SUCCESS:
            logging.error(f"request [{artifact_url}] not succeed [{response.status}]")
            return {}

        # parse repo list from html content
        html_content = response.read()
        # <ul><li"><a href="tag_link>tag (num)</li>...</ul>
        # parse to get tag_link, tag num
        html_struct = etree.HTML(html_content)
        csscase = html_struct.cssselect("#snippets > ul")
        if not csscase:
            logging.error("cannot find <ul> element in html body")
            return {}
        repo_dict = {}
        for li_element in csscase[0].cssselect("li"):
            for a_element in li_element.cssselect("a"):
                # text format: tag (num)
                if not a_element.text:
                    continue
                pattern = r"(.*)[\n ]+\((\d+)\)"
                re_result = re.match(pattern=pattern, string=a_element.text)
                if not re_result:
                    logging.warning(f"{a_element.text} is illegal tag name")
                    continue
                repo_dict[re_result.group(1)] = {}
                repo_dict[re_result.group(1)]["num"] = re_result.group(2)
                try:
                    href = a_element.values()[a_element.keys().index("href")]
                    repo_dict[re_result.group(1)]["href"] = href.strip().split("repo=")[
                        1
                    ]
                    # repo_href
                except ValueError as ve:
                    logging.warning(f'{re_result.group(1)} does not have attr["href"]')
                    repo_dict[re_result.group(1)]["href"] = ""
                except IndexError as ie:
                    logging.warning(f"{href} does not have repo")
                    repo_dict[re_result.group(1)]["href"] = ""
        return repo_dict

    def serch_artifact_version_inrepo(self, group_id: str, artifact_id: str, repo: str):
        """
        parse response from request to ARTIFACT_REPO_TEMPLETE, get jar version provided by maven repo

        attritubes:
            group_id: jar group_id
            artifact_id: jar artifact_id
            repo: repo used in url

        return:
            version_list = {version:date}
        """
        if not group_id or not artifact_id or not repo:
            logging.error(
                f"illegal args: gid[{group_id}] aid[{artifact_id}], repo[{repo}]"
            )
            return {}
        artifact_repo_url = MvnRepositoryAPI.ARTIFACT_REPO_TEMPLETE.format(
            group_id=group_id, artifact_id=artifact_id, repo=repo
        )
        response = self.httpsclient.request_with_defalut_headers(
            "GET", artifact_repo_url
        )
        # HTTP STATUS CODE is not SUCCESS
        if response.status != HTTP_STATUS_SUCCESS:
            logging.error(
                f"request [{artifact_repo_url}] not succeed [{response.status}]"
            )
            return []

        # parse version list from html content
        html_content = response.read()
        # parse to get tag_link, tag num
        html_struct = etree.HTML(html_content)
        tbody_list = html_struct.cssselect(
            "#snippets > div > div > div > table > tbody"
        )

        ver_dict = {}
        for tbody_ele in tbody_list:
            for tr_ele in tbody_ele.cssselect("tr"):
                td_list = tr_ele.cssselect("td")
                print(len(td_list))
                ver_index = 0
                if 5 == len(td_list):
                    ver_index = 1
                elif 4 != len(td_list):
                    logging.error("illegal version format")
                    return []
                date_info = td_list[-1].text.strip()
                ver_info = td_list[ver_index].cssselect("a")[0].text.strip()
                ver_dict[ver_info] = date_info
        return ver_dict


if __name__ == "__main__":
    linq4j_artifict = Artifact("net.hydromatic", "linq4j")
    mvnrepo_conn = MvnRepositoryAPI()
    # repo_list = mvnrepo_conn.serch_artifact(linq4j_artifict.group_id, linq4j_artifict.id)
    # print(repo_list)
    version_list = mvnrepo_conn.serch_artifact_version_inrepo(
        linq4j_artifict.group_id, linq4j_artifict.id, "conjars"
    )
    print(version_list)
