#!/usr/bin/python3
# ******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Author: DisNight
# Create: 2021-08-02
# ******************************************************************************/
"""
maven 项目spec自动生成工具
"""

import argparse
import os
import logging
import datetime
from pymaven.pom import Pom
from artific import HWCMvnRepoMetadateApi

TEMPLATE_SPEC = os.path.join(os.path.dirname(__file__), "mvn_prj.spec")


class MvnPorter(object):
    SPEC_TAG = {
        "NAME",
        "VERSION",
        "RELEASE",
        "SUMMARY",
        "LICENSES",
        "HOMEPAGE",
        "DOWNLOADURL",
        "DESCRIPTION",
        "DATETIME",
        "USERNAME",
        "USEREMAIL",
    }

    def __init__(
        self,
        pom_entry: Pom,
        spec_name="",
        user_name="MavenBot",
        user_email="MavenBot@bot.net",
    ):
        self.pom_entry = pom_entry
        self.user_name = user_name
        self.user_email = user_email
        self.spec_name = spec_name if spec_name else f"{pom_entry.artifact_id}.spec"

    def _get_pom_homepage(self):
        if self.pom_entry._pom_data.find("url") is not None:
            return self.pom_entry._pom_data.find("url").text
        if self.pom_entry._pom_data.find("scm").find("url") is not None:
            return self.pom_entry._pom_data.find("scm").find("url").text
        logging.warning("cannot find homepage in pom.xml")
        return ""

    def _get_pom_licenses(self):
        if self.pom_entry._pom_data.find("licenses"):
            llist = []
            for license in self.pom_entry._pom_data.find("licenses").findall("license"):
                lstr = license.find("name").text
                llist.append(
                    "Apache-2.0" if lstr == "Apache License, Version 2.0" else lstr
                )
            return " and ".join(llist)
        return "UNKNOW"

    def get_all_spec_info(self, check=False):
        spec_info = {}
        spec_info["NAME"] = self.pom_entry.artifact_id
        spec_info["VERSION"] = str(self.pom_entry.version)
        spec_info["RELEASE"] = "1"
        if self.pom_entry._pom_data.find("name") is not None:
            spec_info["SUMMARY"] = self.pom_entry._pom_data.find("name").text
        else:
            logging.warning("cannot get summary from pom xml")
        spec_info["LICENSES"] = self._get_pom_licenses()
        spec_info["HOMEPAGE"] = self._get_pom_homepage()
        # 需要获取对应的下载链接
        spec_info["DOWNLOADURL"] = "@DOWNLOADURL@"
        logging.warning("TODO get source code url")
        if self.pom_entry._pom_data.find("description") is not None:
            spec_info["DESCRIPTION"] = self.pom_entry._pom_data.find("description").text
        else:
            logging.warning("cannot get description from pom xml")
        spec_info["DATETIME"] = MvnPorter.get_time_now("%a %b %d %Y")
        spec_info["USERNAME"] = self.user_name
        spec_info["USEREMAIL"] = self.user_email
        if check:
            not_exist_tag = []
            for tag in MvnPorter.SPEC_TAG:
                if tag not in spec_info:
                    not_exist_tag.append(tag)
            if not_exist_tag:
                nts = " ".join(not_exist_tag)
                logging.warning(f"[{nts}] not exist in spec info")
                return {}
        return spec_info

    def generate_spec(self):
        """
        通过pom信息来生成spec

        attributes:
            output_file:

        return:
            Bool: result to generate spec file
        """
        spec_info = self.get_all_spec_info(True)
        if not spec_info:
            logging.error("cannot get all spec info from pom file")
            return False
        if not os.path.exists(TEMPLATE_SPEC):
            logging.error(f"cannot get templete spec: [{TEMPLATE_SPEC}]")
            return False
        spec_file_content = ""
        with open(TEMPLATE_SPEC, "r") as f:
            spec_file_content = f.read()
        # 替换spec模板中的标签
        for tag in self.SPEC_TAG:
            spec_file_content = spec_file_content.replace(f"@{tag}@", spec_info[tag])
        with open(self.spec_name, "a+") as f:
            f.write(spec_file_content)
        return True

    @staticmethod
    def get_time_now(timeformat="%a %b %d %Y"):
        now_time = datetime.datetime.now()
        return now_time.strftime(timeformat)


def load_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--pomfile", default="", help="path of pomfile")
    # input of new repo name
    parser.add_argument("--groupid", default="", help="groupid of jar file")
    parser.add_argument("--artifactid", default="", help="artifactid of jar file")
    parser.add_argument("--version", default="", help="version of jar file")
    parser.add_argument(
        "-f", "--find_deps_exists", default=False, action="store_true", help="find all"
    )
    parser.add_argument(
        "-d",
        "--download",
        default=False,
        action="store_true",
        help="download source code",
    )
    parser.add_argument("--name", required=True, help="changelog user name")
    parser.add_argument("--email", required=True, help="changelog user email")
    parser.add_argument("-o", "--output", default="", help="path of output spec file")

    args = parser.parse_args()
    return args


def call_mvnporter():
    args = load_args()
    pom_entry = None
    hwc_mvn_repo = HWCMvnRepoMetadateApi()
    if args.pomfile and os.path.exists(args.pomfile):
        with open(args.pomfile, "r") as f:
            pom_entry = Pom(pom_data=f.read())
    elif not args.groupid or not args.artifactid:
        logging.error("input pomfile or [gid:aid] illegal error")
        return
    else:
        pom_entry = hwc_mvn_repo.get_pom(args.groupid, args.artifactid, args.version)
    if not pom_entry:
        logging.error(f"get pom failed")
        return
    logging.info(
        f"start to generate [{pom_entry.group_id}:{pom_entry.artifact_id}:{pom_entry.version}] spec"
    )
    mvnporter = MvnPorter(pom_entry, args.output, args.name, args.email)

    ret = mvnporter.generate_spec()
    if ret:
        logging.info(
            f"generate [{pom_entry.coordinate}] spec [{mvnporter.spec_name}] succeed"
        )
    else:
        logging.error(f"generate [{pom_entry.coordinate}] spec failed")


if __name__ == "__main__":
    call_mvnporter()
