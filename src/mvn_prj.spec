%define artifict_name @NAME@

Name:          %{artifict_name}
Version:       @VERSION@
Release:       @RELEASE@
Summary:       @SUMMARY@
License:       Apache 2.0
URL:           @HOMEPAGE@

Source0:       @DOWNLOADURL@

BuildRequires: java-1.8.0-openjdk-devel maven maven-local
Requires: java-1.8.0-openjdk
BuildArch:     noarch

%description
@DESCRIPTION@

%package help
Summary:    Javadoc for %{name}
Provides:   %{name}-javadoc = %{version}-%{release}
Obsoletes:  %{name}-javadoc < %{version}-%{release}
%description help
This package contains javadoc for %{name}

%prep
%autosetup

%build
%mvn_build -b -f

%install
%mvn_install

%files -f .mfiles

%files help -f .mfiles-javadoc

%changelog
* @DATETIME@ @USERNAME@ <@USEREMAIL@> @VERSION@-@RELEASE@
- Init package
